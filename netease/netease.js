var proxy = "SOCKS5 127.0.0.1:1081; SOCKS 127.0.0.1:1081; DIRECT;";
// var proxy = "SOCKS5 127.0.0.1:1080; SOCKS 127.0.0.1:1080; DIRECT;";

var domains = [
    "music.163.com",
    "interface.music.163.com",
];

function FindProxyForURL(url, host) {
    for (var i = domains.length - 1; i >= 0; i--) {
        if (dnsDomainIs(host, domains[i])) {
            return proxy;
        }
    }
    return "DIRECT";
}
